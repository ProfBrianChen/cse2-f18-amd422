//Adam Debus 10/11/18
//Prof. Carr CSE002 lab06
//This code will generate a pyramid pattern based on the length the user indicates

import java.util.Scanner;
public class PatternC
{
    public static void main (String [] args)
    {
        Scanner keyboard = new Scanner (System.in);//declares scanner as keyboard for later use
        System.out.println("Please enter the length of you pyramid from 1 - 10: "); //asks user for input of rows
        int numRows; //declares number of rows
       
            while (true)
            {
                boolean correct = keyboard.hasNextInt(); //Goes into loop and checks if int is entered
                if (correct)
                {
                    numRows = keyboard.nextInt(); //IF int is enetered, the int is printed
                    break; //used to leave if statement/loop
                }
                else //when the incorrect type is entered
                {   
                    System.out.println("Please enter the correct type.");
                    keyboard.next(); //commands the user to try again
                }
            }
            while (numRows < 1 || numRows > 10) //checks if number enetred is within range
            {
                System.out.println("Please enter an integer within range.");//asks for integer within range after failed attempt
                numRows = keyboard.nextInt();
                if (numRows > 0 && numRows < 11) //checks if number is within range
                {
                    
                    break;
                    //used to leave if statement/loop
                }
                else
                {
                    System.out.println("Please enter an integer within range.");
                    keyboard.next(); //commands the user to try again
                }
            }
            
            System.out.println("Your pyramid length is " + numRows); //states pyramid length so user knows what he entered
            
            
            for (int i = 1; i <= numRows; i++) //starts at 1 and goes all the way up to numRows
            {
                for(int k=numRows; k>=i; k--) 
                {
                    System.out.print(" "); //prints the blank space before each line
                }
                for (int j = i; j > 0; j--) //used to count down as the loop progresses
                {
                    System.out.print("" + j); // prints j with a space following
                }
                System.out.println("");//used to go to next line
            }
        
    }
}