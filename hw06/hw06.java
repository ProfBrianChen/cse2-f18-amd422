//Adam Debus 10/18/18
//Prof. Carr CSE2
//This program uses stars to encrypt an X on the display screen using a nested for loop

import java.util.Scanner;
public class hw06
{
    public static void main (String [] args)
    {
        Scanner keyboard = new Scanner (System.in);//declares scanner as keyboard for later use
        System.out.println("Please enter an integer from 1 - 100: "); //asks user for input of the size
        int myInt; //declares number to be used as the size of the X
       
            while (true)
            {
                boolean correct = keyboard.hasNextInt(); //Goes into loop and checks if int is entered
                if (correct)
                {
                    myInt = keyboard.nextInt(); //IF int is entered, the int is printed
                    break; //used to leave if statement/loop
                }
                else //when the incorrect type is entered
                {   
                    System.out.println("Please enter the correct type.");
                    keyboard.next(); //commands the user to try again
                }
            }
            while (myInt < 1 || myInt > 100) //checks if number entred is within range
            {
                System.out.println("Please enter an integer within range.");//asks for integer within range after failed attempt
                myInt = keyboard.nextInt();
                if (myInt > 0 && myInt < 101) //checks if number is within range
                {
                    
                    break;
                    //used to leave if statement/loop
                }
                else
                {
                    System.out.println("Please enter an integer within range.");
                    keyboard.next(); //commands the user to try again
                }
            }
            
            System.out.println("Your size is " + myInt); //states size so user knows what he entered
            
           for (int rows = 1; rows <= myInt; rows++)  //for loop used to go through the rows
           {
                for (int columns = 1; columns <= myInt; columns++) //for each row the column goes through however many times the user inputs
                {
                    if (columns == rows || columns == myInt - rows + 1) //checks when the column position equals the row position, also check when the column position is equal to the row position going the opposite direction
                    {                                                        // when either one of these statements is true a space is put in its place (used to make the X)
                        System.out.print(" ");
                    }
                    
                    else 
                    {
                        System.out.print("*"); //an asterik is put in all other spaces to make the outline of the X and fill the myInt by myInt grid
                    }
                    
                }
                System.out.println(); //prints a blank new line for the new row to start on
            }
    }
}