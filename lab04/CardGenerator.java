//Adam Debus CSE002
//Prof. Carr 9/20/18
//This code randomly generates a number from 1-52 (inclusive) that corresponds to a certain card in a standard deck of cards

public class CardGenerator{
  
  public static void main (String [] args){
    //Declares the three variables to be used throughout calculation
    int num = (int)(Math.random()*52)+1;
    String suitName = "";
    String cardIdentity;
    //Uses a switch to determine the name of the suit
    switch(num%4){
      case(0):
        suitName = "Clubs";
        break;
      case(1):
        suitName = "Diamonds";
        break;
      case(2):
        suitName = "Hearts";
        break;
      case(3):
        suitName = "Spades";
        break;
    }
    //Uses a switch to determine the identity of a card
    switch (num%13){
      case (0):  
        cardIdentity = "Ace";
        System.out.println("You picked a " + cardIdentity + " of " + suitName);
        break;
      case (1):
        cardIdentity = "2";
        System.out.println("You picked a " + cardIdentity + " of " + suitName);
        break;
      case (2): 
        cardIdentity = "3";
        System.out.println("You picked a " + cardIdentity + " of " + suitName);
        break;
      case (3):
        cardIdentity = "4";
        System.out.println("You picked a " + cardIdentity + " of " + suitName);
        break;
      case (4):
        cardIdentity = "5";
        System.out.println("You picked a " + cardIdentity + " of " + suitName);
        break;
      case (5):   
        cardIdentity = "6";
        System.out.println("You picked a " + cardIdentity + " of " + suitName);
        break;
      case (6): 
        cardIdentity = "7";
        System.out.println("You picked a " + cardIdentity + " of " + suitName);
        break;
      case (7):
        cardIdentity = "8";
        System.out.println("You picked a " + cardIdentity + " of " + suitName);
        break;
      case (8):
        cardIdentity = "9";
        System.out.println("You picked a " + cardIdentity + " of " + suitName);
        break;
      case (9):
        cardIdentity = "10";
        System.out.println("You picked a " + cardIdentity + " of " + suitName);
        break;
      case (10):
        cardIdentity = "Jack";
        System.out.println("You picked a " + cardIdentity + " of " + suitName);
        break;
      case (11):
        cardIdentity = "Queen";
        System.out.println("You picked a " + cardIdentity + " of " + suitName);
        break;
      case (12):
        cardIdentity = "King";
        System.out.println("You picked a " + cardIdentity + " of " + suitName);
        break;
        
      
        
    }
    
  }
}