//Adam Debus 11/10/18
//Prof. Carr CSE002
//This program prints a deck of 52 cars then shuffles them, then pulls out a random hand from the end of the shuffled deck
import java.util.Scanner;

public class hw08 {

    public static void main(String[] args) 
    {
        Scanner keyboard = new Scanner(System.in);//decalres scanner as keyboard
        //suits club, heart, spade or diamond 
        String[] suitNames = {"C", "H", "S", "D"};//creates an array of strings that represent card names
        String[] rankNames = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};//array of strings that represents card numbers/type
        String[] cards = new String[52];//initializes a string of size 52 (51 indexes) to be used for the hand
        String[] hand = new String[5];//creates a string of 5 spaces (4 indexes) to be used for the hand
        int numCards = 5;//sets the size of the hand equal to five
        int again = 1;//used to set the while loop to true so it initially runs through at least once
        int index = 51;//sets the index to be used in the getHand method equal to the last index of cards
        for (int i = 0; i < 52; i++) //for loop that iterates from 0-51
        {
            cards[i] = rankNames[i % 13] + suitNames[i / 13];//each one of the 52 times this line is used, the ranknames and suitnames are spliced together and placed into a position of cards
            //System.out.print(cards[i] + " "); Left out print statement because of redundancy
        }
        //System.out.println();
        System.out.println(printArray(cards));//calls the method printArray which prints out what is stored at each index
        String[] cards1 = shuffle(cards);//sets what is returned from the method shuffle to cards1
        System.out.println(printArray(cards1));//prints thus using the same method as before
        while (again == 1) //evaluates to true because of what was mdeclared above
        {
            hand = getHand(cards, index, numCards);//calls the method getHand
            System.out.println(printArray(hand));//prints hand as using same technique as mentioned above
            index = index - numCards;//moves the index down by numcards to chnage what hand you get the next time around
            System.out.println("Enter a 1 if you want another hand drawn");//asks the user if they want another hand
            again = keyboard.nextInt();//takes in user input
        }
    }
    
    public static String printArray(String[] list)//one parameter that is an array
    {
        String newList = "";//decalres a string called newList
        for (int i = 0; i < list.length; i++)//runs through the array that was used in calling the method
        {
            newList += list[i] + " ";//adds what is in each index to the string newList
        }
        return newList;//returns the string
        
    }
    
    public static String[] shuffle(String[] list)//one parameter that is an array
    {
        int randomNum;//deaclares an int randomNum
        String temp;//declares a string
        for (int i = 0; i <1000; i++)
        {
            randomNum = (int)(Math.random() * 51);//generates a random number from 0-51
            temp = list[0];//sets the value in list[0] equal to temp
            list[0] = list[randomNum];//sets the value in list[randomNum] equal to list[0]
            list[randomNum] = temp;//sets temp equal to list[randomNum]       THESE PAST THREE LINES ARE NECESSARY FOR A VALUE SWAP
            
        }
        System.out.println("Shuffled");
        return list;//retuns the array to be used in the main method
    }
    
    public static String[] getHand(String[] list, int index, int numCards)//three parameters of an array, and two ints
    {
        String[] newList1 = new String[5];//creates a new array that is set at indexes 0-4
        int j = 0;//initializes j at 0 which is to be used as a counter
        for (int i = index; i > index-5; i--)//for loop that goes through indexes from number 51-->(51-5)
        {
            newList1[j] = list[i];//sets the value at that index of list equal to the index j of newList1
            j++;//increments j to move along newList1
        }
        System.out.println("Hand");
        return newList1;//return the new array to be sued in the main method
    }
}