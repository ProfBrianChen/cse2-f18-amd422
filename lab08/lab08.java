//Adam Debus 11/08/18
//Prof. Carr CSE2
//This program will tell the user the number of occurences of each number in an array of 100 randomly generated integers

import java.util.Arrays; //imports arrays to use when printing out the array on line18
public class lab08
{
    public static void main (String [] args)
    {
        int[] array1 = new int[100];//declares array1 with index 0-99
        int[] array2 = new int[100];//declares array2 with index 0-99
        
        for (int i = 0; i < 100; i++)//for loop that goes through each spot in array
        {
            array1[i] = (int) (Math.random() * 100 - 1);//generates a random number form 0 - 99 
        }
        
        System.out.println("Array 1 holds the following integers: " + Arrays.toString(array1));//prints out randomly generated array
        
        for (int i = 0; i < 100; i++)//for lopp that goes through vales 0 - 99
        {
            for (int j = 0; j < 100; j++)//for loop that goes through each element in array1
            {
                if (i == array1[j])//checks if the vlaue is equal to the element in array1
                {
                    array2[i]++;//if so, it incrememts that space in array 2 which is zero
                }      
            }
            if (array2[i] != 0)//check if the space in array2 is not equal to zero, in order to dtermine if it should print it
            {
                System.out.println(i + " occurs " + array2[i] + " times");//when not equal to zero, this executes and prints the occurences of each number
            } 
        }
    }
}
