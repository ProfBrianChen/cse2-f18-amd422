//Adam Debus 10/25/18
//Prof. Carr CSE002
//This program creates a class that uses different methods to print random sentences that somewhat make sense
import java.util.Random;
import java.util.Scanner;
public class Methods
{
    public static void main (String [] args)
    {
        Scanner keyboard = new Scanner (System.in); //declares scanner as keyboard
        System.out.println("Here is your first sentence: \n" + Sentence()); //gives user initial sentence
        System.out.println("Would you like another sentence? (Enter 1 for Yes, 2 for No)"); //assuming user only enters a 1 or 2, it determines if the user would like another sentence
        int myInt = keyboard.nextInt(); // gets user input
        while (myInt == 1)//checks if the user wants another sentence
        {
            System.out.println(Sentence());//prints that sentence
            System.out.println("Would you like another sentence? (Enter 1 for Yes, 2 for No)");//asks again in an infinite loop, until the user inputs a number other than 1
            myInt = keyboard.nextInt(); //takes in user's inpout
        }
        
        System.out.println("Would you like a whole paragraph? (Enter 1 for Yes, 2 for No)"); //asks user of they want a paragraph
        int myInt2 = keyboard.nextInt(); //takes in the user's input
        if (myInt2 == 1)//checks if the user wants the paragraph
            System.out.println(Paragraph());//prints the paragraph
        else
            System.out.println("No paragraph");//prints when the user does not want a paragraph
      
        
    }
    
    public static String Adjective()
    {
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        //(This statement generates random integers less than 10)
        String randomAdj = ""; //deaclares adjective
        switch(randomInt)//uses random number to generate one of the adjectives that follow
        {
            case 0:
                randomAdj = "funny";
                break;
            case 1:
                randomAdj = "weird";
                break;
            case 2:
                randomAdj = "excellent";
                break;
            case 3:
                randomAdj = "awesome";
                break;
            case 4:
                randomAdj = "cool";
                break;
            case 5:
                randomAdj = "smart";
                break;
            case 6:
                randomAdj = "kind";
                break;
            case 7:
                randomAdj = "tall";
                break;
            case 8:
                randomAdj = "scary";
                break;
            case 9:
                randomAdj = "spooky";
                break;
        }
        return randomAdj; //returns adjective

    }
    
    public static String Subject()
    {
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        //(This statement generates random integers less than 10)
        String randomSubject = ""; //declares sunject
        switch(randomInt)//uses a switch to determine what subject to be used based off the random number
        {
            case 0:
                randomSubject = "cat";
                break;
            case 1:
                randomSubject = "dog";
                break;
            case 2:
                randomSubject = "plane";
                break;
            case 3:
                randomSubject = "horse";
                break;
            case 4:
                randomSubject = "car";
                break;
            case 5:
                randomSubject = "guerilla";
                break;
            case 6:
                randomSubject = "turtle";
                break;
            case 7:
                randomSubject = "whale";
                break;
            case 8:
                randomSubject = "shark";
                break;
            case 9:
                randomSubject = "emu";
                break;
        }
        return randomSubject; //returns subject
    }
    
    public static String pasttenseVerb()
    {
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        //(This statement generates random integers less than 10)
        String pasttenseVerb = "";//declares past tense verb
        switch(randomInt)//uses swithc to determine which verb to use based off random number
        {
            case 0:
                pasttenseVerb = "ran to";
                break;
            case 1:
                pasttenseVerb = "ate";
                break;
            case 2:
                pasttenseVerb = "pet";
                break;
            case 3:
                pasttenseVerb = "talked to";
                break;
            case 4:
                pasttenseVerb = "payed";
                break;
            case 5:
                pasttenseVerb = "walked to";
                break;
            case 6:
                pasttenseVerb = "asked";
                break;
            case 7:
                pasttenseVerb = "couldn't eat";
                break;
            case 8:
                pasttenseVerb = "watched";
                break;
            case 9:
                pasttenseVerb = "played";
                break;
        }
        return pasttenseVerb; //returns verb
    }
    
    public static String Object()
    {
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(10);
        //(This statement generates random integers less than 10)
        String object = ""; //decalres object
        switch(randomInt)//uses switch to determine subject based off randome number
        {
            case 0:
                object = "guy";
                break;
            case 1:
                object = "girl";
                break;
            case 2:
                object = "bear";
                break;
            case 3:
                object = "beaver";
                break;
            case 4:
                object = "bird";
                break;
            case 5:
                object = "beetle";
                break;
            case 6:
                object = "bat";
                break;
            case 7:
                object = "anteater";
                break;
            case 8:
                object = "bobcat";
                break;
            case 9:
                object = "buffalo";
                break;
        }
        return object; //returns object
    }
    
    public static String Sentence() 
    {
        String mySentence = " ";//decalres sentence
        mySentence = "The " + Adjective() + " " + Subject() + " " + pasttenseVerb() + " the " + Adjective() + " " + Object(); //creates sentence that is to be returned in the next line
        return mySentence; //returns first sentence
    }
    
    public static String thesisSentence()
    {
        String mySentence = " "; //decalres next sentence of paragraph
        mySentence = "It also " + pasttenseVerb() + " the " + Adjective() + " " + Object();//builds sentece using it to refer to previous subject
        return mySentence; //returns next sentence
    }
    
    public static String Conclusion()
    {
        String mySentence = " "; //decalres last sentence
        mySentence = "Fianlly, it " + pasttenseVerb() + " the " + Adjective() + " " + Object();//used finally to end off paragraph similar to previous sentence
        return mySentence; //returns conclusion sentence
    }
    
    public static String Paragraph()
    {
        String myPara = " "; //declares the paragraph
        myPara = Sentence() + "\n" + thesisSentence() + "\n" + Conclusion(); //puts together the paragraph on separate lines
        return myPara;// returns paragraph;
    }
}