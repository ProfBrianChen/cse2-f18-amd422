//Adam Debus September 13th, 2018
// CSE2 Prof. Carr
/*This program will obtain from the user the original cost of the check,
the percentage tip they wish to pay, and the number of ways the check will be split. 
Then it will calculate how much each person is paying in the split*/

//This imports the Scanner
import java.util.Scanner;

public class Check{
  
  public static void main (String [] args){
    
    //This declares the scanner as "keyboard"
    Scanner keyboard = new Scanner(System.in);
    
    //The next two lines asks the user to input the cost of the bill and accepts that input
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    double checkCost = keyboard.nextDouble();
    
    //The next two lines asks the user to input the percent tip they would like to pay
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = keyboard.nextDouble();
    tipPercent /= 100; //We want to convert the percentage into a decimal value
    
    
    //The next two lines askes and accepts the input for the number of people at dinner  
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = keyboard.nextInt();
    
    
    double totalCost;
    double costPerPerson;
    int dollars,   //whole dollar amount of cost 
    dimes, pennies; //for storing digits to the right of the decimal point for the cost$ 
    
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    //get the whole amount, dropping decimal fraction
    dollars = (int)costPerPerson;
    
    //get dimes amount, e.g., 
    // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
    //  where the % (mod) operator returns the remainder
    //  after the division:   583%100 -> 83, 27%5 -> 2 
    dimes = (int)(costPerPerson * 10) % 10;
    
    //Gets the number of pennies at the hundreds place
    pennies = (int)(costPerPerson * 100) % 10;
    
    
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies); 


    
    
    
    
  }
}