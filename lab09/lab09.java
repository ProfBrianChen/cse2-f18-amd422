//Adam Debus 11/15/18
//Prof. Carr CSE002
//This program will have different methods that perform different tasks to arrays to show the impoortance of passing by value

import java.util.Arrays;
public class lab09
{
    public static void main (String[] args)
    {
        int[] array0 = {1, 2, 3, 4, 5, 6, 7, 8, 9}; //creates array of ints to be tested
        int[] array1 = copy(array0); //using the method copy, array 1 is assigned to the same values as array0, but not in the same location
        int[] array2 = copy(array0); //using the method copy, array 2 is assigned to the same values as array0, but not in the same location
        inverter(array0); //inverts the original array using the inverter method below
        print(array0); //uses the print method to print the original array
        inverter2(array1); //uses the second inverter method that passes a one of the copied arrays as a parameter 
        print(array1); //uses the print methid to print this modified array1
        int [] array3 = inverter2(array2);//uses inverter2 on array2 then assigns that value to another array
        print(array3);//prints out the newly created array
    }
    
    public static int[] copy(int[] myArray)//takes in an interger array as a parameter
    {
        int[] newArray = new int[myArray.length];//declares a new array that is to be modified 
        for (int i = 0; i < myArray.length; i++)//for loop that interates through the length of the array
        {
            newArray[i] = myArray[i];//assigns the value from the original array to the new array
        }
        return newArray; //returns the new array with th changes made to it
    }
    
    public static void inverter(int[] myArray)//takes in an integer array as a parameter
    {
        int temp;//declares an int temp to be used in the swap
        int n = myArray.length;//for easier code readability, a varibale n is assigned to the length of the array to be used below
        for (int i = 0; i < n/2; i++)//for loop that goes through half of the values of the array
        {
            temp = myArray[i];//sets the value at the specific index of myArray equal to temp
            myArray[i] = myArray[n - 1 - i];//sets the opposite element to be swapper with equal to the index of where it needs to be swapped
            myArray[n - 1 - i] = temp;//puts the value of temp (original value of the specific index of the original array) in the corressponding location for the swap
        }
    }
    
    public static int[] inverter2(int[] myArray)//takes in an integer array as a parameter
    {
        int[] newArray = copy(myArray);//creates a new array that is a copy of the array sent in as a parameter
        inverter(newArray); //uses the inverter methid above to reverse the array sent in as a parameter
        return newArray;//retuns that array
    }
    
    public static void print(int[] myArray)//print method that takes in an integer array parameter
    {
        System.out.println(Arrays.toString(myArray));//uses the import array class to print the array using the toString method
    }
}