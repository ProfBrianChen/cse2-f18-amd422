//Adam Debus 9/16/18
//Prof Carr CSE2
//This code converst double values into cubic miles

import java.util.Scanner;
public class Convert{
  
  public static void main (String [] args){
    
    Scanner keyboard = new Scanner(System.in);//Declares scanner as keyboard
    System.out.println("Enter the affected area in acres: ");//Asks for input of area affected
    double acres = keyboard.nextDouble();
    
    System.out.println("Enter the rainfall in the affected area in inches: ");//Asks for inches of rainfall
    double rainfall = keyboard.nextDouble();
    
    double cubicMiles = (rainfall/63360)*(acres*.0015625);//Converts input numbers to cunic miles knowing that there are 63360 inches in a mile and .0015625 squared miles in an acre
    
    System.out.println(cubicMiles + " cubic miles");//Prints out answer
    
    
  }
}