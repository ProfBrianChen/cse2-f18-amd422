//Adam Debus 9/16/18
//Prof. Carr CSE2
//This program prompts the user for the dimensions of a pyramid and returns the volume inside the pyramid
import java.util.Scanner;
public class Pyramid{
  
  public static void main (String [] args)
  {
    Scanner keyboard = new Scanner(System.in);//Declares scanner as keyboard
    System.out.println("The square side of the pyramid is (input length): ");//Asks for side length
    double length = keyboard.nextDouble();
    
    System.out.println("The height of the pyramid is (input height): ");//Asks for height
    double height = keyboard.nextDouble();
    
    double volume = (length*length)*(height/3.0);//Uses height and side length in the forumla of volume of a pyramid
    System.out.println("The volume of the pyramid is " + volume);//Prints out the answer above
    
  }
}

