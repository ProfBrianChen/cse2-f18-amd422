//Adam Debus 11/23/18
//Prof. Carr CSE2
//This program removes different elements of a random array by using different methods

import java.util.Scanner;

public class RemoveElements {

    public static void main(String[] arg) {       //********LINES 9-52 WERE PROVIDED BY PROF. CARR FOR THE ASSIGNMENT*********
        Scanner scan = new Scanner(System.in);
        int num[] = new int[10];
        int newArray1[];
        int newArray2[];
        int index, target;
        String answer = "";
        do {
            System.out.print("Random input 10 ints [0-9]");
            num = randomInput();
            String out = " The original array is:";
            out += listArray(num);
            System.out.println(out);

            System.out.print("Enter the index ");
            index = scan.nextInt();
            newArray1 = delete(num, index);
            String out1 = "The output array is ";
            out1 += listArray(newArray1); //return a string of the form "{2, 3, -9}"  
            System.out.println(out1);

            System.out.print("Enter the target value ");
            target = scan.nextInt();
            newArray2 = remove(num, target);
            String out2 = "The output array is ";
            out2 += listArray(newArray2); //return a string of the form "{2, 3, -9}"  
            System.out.println(out2);

            System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
            answer = scan.next();
        } while (answer.equals("Y") || answer.equals("y"));
    }

    public static String listArray(int num[]) {    //Method provided by Prof. Carr that prints out the array
        String out = "{";
        for (int j = 0; j < num.length; j++) {
            if (j > 0) {
                out += ", ";
            }
            out += num[j];
        }
        out += "} ";
        return out;
    }
    
    public static int[] randomInput()
    {
        int[] myArray = new int[10];//creates an array of indexes 0-9
        for (int i = 0; i < 10; i++)//for loop that goes through array
        {
            myArray[i] = (int)(Math.random() * 10);//generates random number from 0-10 to enter in array
        }
        
        return myArray;//retusn array
    }
    
    public static int[] delete(int[] list, int pos) 
    {
        int[] myArray = new int[list.length - 1];//declares a new array of 1 less length b/c one index will be removed
        if (pos > -1 && pos < list.length) //checks if pos is valid
        {
            int counter = 0;
            for (int i = 0; i < list.length; i++) //for loop that iterates through the original array
            {
                if (i != pos) //if the position deired is not equal to the index that value will be added to the array
                {
                    myArray[counter] += list[i];
                    counter++;
                }
            }
            return myArray;
        }
        else //what happens if the position is not valid
            System.out.println("The index is not valid");
            return list;
    }
    
    public static int[] remove(int[] list, int target)
    {
        int counter = 0;//establishes counter to be used to keep track of number of instances where list[i] = target
        for (int i = 0; i < list.length; i++)//iterates through length of original array
        {
            if (list[i] == target)
            {
                counter++;//keeps track of number of instances where list[i] = target
            }
        }
        if (counter > 0)
                System.out.println("Element " + target + " was found.");//print statements that tell the user whether or not their element was found based on the value of the counter
            else if(counter <= 0)
                System.out.println("Element " + target + " was not found.");
        
        int counter2 = 0;//new counter that keeps track of indexes
        int[] myArray = new int[list.length-counter];//creates an array of the length of the previous one minus the number of spaces that need to be removed
        for (int j = 0; j < list.length; j++)
        {
            if (list[j] != target)//checks if the position in list has the same value as the target value
            {
                myArray[counter2] += list[j];//if so, it is added to the new array 
                counter2++;//counter that keeps track of spot in new array
            }
        }
        return myArray;//returns array
    }
}

