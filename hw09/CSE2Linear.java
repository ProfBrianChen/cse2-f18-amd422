//Adam Debus 11/23/18
//Prof Carr CSE2
//This program uses both a binary and linear seach to search for a specified value in an array
import java.util.Scanner;
import java.util.Arrays;

public class CSE2Linear
{
    public static void main (String[] args)
    {
        Scanner keyboard = new Scanner (System.in);//declares scanner as keyboard for later use
        System.out.println("Please enter 15 integers from 0 - 100: "); //asks user for input of the size
        int myInt; //declares number to be used as the size of the X
        int myArray[] = new int[15];
        for(int j = 0; j < 15; j++)
        {
            while (true)
            {
                boolean correct = keyboard.hasNextInt(); //Goes into loop and checks if int is entered
                if (correct)
                {
                    myInt = keyboard.nextInt(); //If int is entered, the int is printed
                    break; //used to leave if statement/loop
                }
                else //when the incorrect type is entered
                {   
                    System.out.println("Please enter the correct type.");
                    keyboard.next(); //commands the user to try again
                }
            }
            while (myInt < 0 || myInt > 100) //checks if number entred is within range
            {
                System.out.println("Please enter an integer within range.");//asks for integer within range after failed attempt
                myInt = keyboard.nextInt();
                if (myInt > -1 && myInt < 101) //checks if number is within range
                {
                    
                    break;
                    //used to leave if statement/loop
                }
                else
                {
                    System.out.println("Please enter an integer within range.");
                    keyboard.next(); //commands the user to try again
                }
            }
            if (myArray[0] == 0)
            {
                myArray[j] = myInt;
            }
            else if (myInt > myArray[j-1])
            {
                myArray[j] = myInt;
            }
            else
            {
                System.out.println("Please enter an integer greater than the previous one.");
                myInt = keyboard.nextInt(); //commands the user to try again
                myArray[j] = myInt;
            }
            
            System.out.println(Arrays.toString(myArray));//prints array after each number is entered for clarity
        }
        
        System.out.println("Please enter an integer you would like to binary search for.");
        int searchInt = keyboard.nextInt();//takes in int as search value
        int myBinary = binary(myArray, searchInt);//runs the binary seach method
        if (myBinary == -1)
            System.out.println("The desired integer was not found.");//prints if the integer was not found
        else
            System.out.println("The desired integer was found at index " + myBinary);
        
        System.out.println(Arrays.toString(shuffle(myArray)));
        
        System.out.println("Please enter an integer you would like to linear seach for.");
        int searchInt2 = keyboard.nextInt();//runs the method for the linear search
        int myLinear = linearSearch(myArray, searchInt2);
        if (myLinear == -1)
            System.out.println("The desired integer was not found.");//prints if integer was not found
        else
            System.out.println("The desired inetger was found at index " + myLinear);
    }
  
    public static int binary(int[] list, int key) 
    {
        //This code is taken from the code that was developed by Prof. Carr and her students in class
        // Note that this implementation DOES loop through
        // and continue to search for an element.  This is
        // our second, and correct, implementation of binary 
        // search
        int iterationCounter = 1;
        int low = 0;
        int high = list.length - 1;
        while (high >= low) {
            int mid = (low + high) / 2;
            if (key < list[mid]) 
            {
                high = mid - 1;
            }
            else if (key == list[mid]) 
            {
                System.out.println(iterationCounter + " iterations");
                return mid;
            } 
            else 
            {
                low = mid + 1;
            }
            iterationCounter++;
        }
        System.out.println(iterationCounter + " iterations");
        return -1;
        //return -low - 1;
        // When a key is not found, low is the insertion point where
        // a key would be inserted to maintain the order of the list.
    }

    public static int linearSearch(int[] list, int key) 
    {
        //This code was also taken from the shared file that was created by Prof. Carr and her class
        int iterationCounter = 1;
        for (int i = 0; i < list.length; i++) 
        {
            if (key == list[i]) 
            {
                System.out.println(iterationCounter + " iterations");
                return i;//retuns when the key is found, returns the index
            }
            iterationCounter++;
        }
        System.out.println(iterationCounter + " iterations");//notfication of iterations
        return -1;//when key is not found
    }
    
    public static int[] shuffle(int[] list)//one parameter that is an array
    {
        int randomNum;//deaclares an int randomNum
        int temp;//declares a string
        for (int i = 0; i <1000; i++)
        {
            randomNum = (int)(Math.random() * 14);//generates a random number from 0-51
            temp = list[0];//sets the value in list[0] equal to temp
            list[0] = list[randomNum];//sets the value in list[randomNum] equal to list[0]
            list[randomNum] = temp;//sets temp equal to list[randomNum]       THESE PAST THREE LINES ARE NECESSARY FOR A VALUE SWAP
            
        }
        return list;//retuns the array to be used in the main method
    }

}
