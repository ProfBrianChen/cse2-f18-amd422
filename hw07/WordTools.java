//Adam Debus 10/25/18
//Prof. Carr CSE002
//This program takes a user input of a string and performs different edits to it based on what methods the user calls
import java.util.Scanner;//imports scanner class
public class WordTools
{
    public static void main (String [] args)
    {
        Scanner keyboard = new Scanner (System.in); //declares scanner as keyboard
        String text = sampleText();//runs the method sampleText that takes in the users string
        System.out.println("Your string is: '" + text + "'");//prints out users string
        boolean condition = true;//sets while loop to start at true
        while (condition) 
        {
            System.out.println();//prints a blank line for visual effect
            char choice = printMenu();//runs the method printMenu that returns what action the user would like
            switch (choice) //switch statement that determines what actions to take based off of the usres inputted 
            {
                case 'c':
                    int numNonWS = getNumOfNonWSCharacters(text);//runs methods
                    System.out.println("Number of Non Whitespace Character: " + numNonWS);//prints out result
                    break;
                case 'w':
                    int numWords = getNumOfWords(text);//runs method
                    System.out.println("Number of words: " + numWords);//prints out result
                    break;
                case 'f':
                    System.out.println("Please input the word you would like to find within your previously inputted string: ");//asks user for word they would like to find
                    String word = keyboard.next();//declares scanner as keyboard again because of scoping
                    int numOccurences = findText(text, word);//runs method
                    System.out.println("'" + word + "' instances: " + numOccurences);//prints method
                    break;
                case 'r':
                    String replEx = replaceExclamation(text);//runs method
                    System.out.println("Edited string: \n" + replEx);//prints method
                    break;
                case 's':
                    String shortSpace = shortenSpace(text);//runs method
                    System.out.println("Edited string: \n" + shortSpace);//prints method
                    break;
                case 'q':
                    String quit = Quit();//runs method
                    System.out.println(quit);//prints method
                    condition = false;//stops the while loop
                    break;
                default:
                    System.out.println("Not an available option. Try again.");//prints when user inputs an invailid option
                    break;
            }
        }
    }
    
    public static String sampleText()
    {
        Scanner keyboard = new Scanner (System.in); //declares scanner as keyboard
        System.out.println("Please enter a string of your choosing: "); //asks for string
        String text = keyboard.nextLine();//takes in string
        return text;//retusn string
    }
    
    public static char printMenu()
    {
        Scanner keyboard = new Scanner (System.in); //declares scanner as keyboard
        System.out.println("MENU"); 
        System.out.println("c - Number of non-whitespace characters"); //prints the menu as seen in the hw07 document
        System.out.println("w - Number of words");
        System.out.println("f - Find text");
        System.out.println("r - Replace all !'s");
        System.out.println("s - Shorten spaces");
        System.out.println("q - Quit");
        System.out.println();
        System.out.println("Choose an option: ");
        char choice = keyboard.next().charAt(0); //after printing the menu, this takes in the character value that the user inputs
        return choice;
    }
    
    public static int getNumOfNonWSCharacters(String text)
    {
        int counter = 0;    
        for (int i = 0; i < text.length(); i++)
        {
            if (text.charAt(i) == ' ') //checks if the space is a whitespace
            {
                counter++;//counts the whitespaces
            }
        }
        return (text.length() - counter);//the length of the string minus the number of white spaces is the number of non whitespaces
    }
    
    public static int getNumOfWords(String text)
    {
        int counter = 0;    
        for (int i = text.length() - 1 ; i >= 0; i--)
        {
            if (text.charAt(i) == ' ' && text.charAt(i-1) != ' ')//check for white space without another whitespace behind it
            {
                counter++;//counts the number of these occurences
            }
        }
        return counter + 1;//returns the counter plus one because the last word would not end in a space
    }
    
    public static int findText(String text, String word)
    {
        int counter = 0;
        int index = 0;
        while ((index = text.indexOf(word, index)) != -1) //searches to check if the string is present, a -1 would occur when the value is not present
        {
            counter++;
            index += word.length();//advances forward to keep counting
        }
        return counter; //returns the number of instances of the word
    }
    
    public static String replaceExclamation(String text)
    {
        String modText = "";//creates new string to be usde in concatenating the charcters
        for (int j = 0; j < text.length(); j++)//runs through the charcters in text
        {
            if (text.charAt(j) != '!')//if its not an ! the 
                                        //code just adds the regualr character to modText 
            { 
                modText += text.charAt(j);
            }
            else if (text.charAt(j) == '!') //if it is an ! then the code adds 
            {                                 // a period in its place to the new string
                modText += ".";
            }
        }
        return modText;//retusn the new string
        
    }
    
    public static String shortenSpace(String text)
    {
        String modText = "";//creates a new string to be concatenated together
        boolean condition = false;//uses a boolean to determine which if statement to enter
        for (int j = 0; j < text.length(); j++)//goes through the charcters in text
        {
            if (text.charAt(j) != ' ')//checks if a charcter is not equal to 'space'
            {
                condition = false;
                modText += text.charAt(j); //just continues regularly wihtout changes by adding previous characters to the new string
            }
            else if (text.charAt(j) == ' ' && !condition) //checks if there is space and the boolean is false from previous if
            {
                condition = true;
                modText += " ";//adds a normal space to where the numtiple spaces were
            }
        }
        return modText;//returns the new string
        
    }
    
    public static String Quit()
    {
        return "WordTools has now ended.";//prints a statement that ends code
    }
}
