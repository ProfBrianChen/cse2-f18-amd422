//Adam Debus 10/5/18
//CSE2 Prof. Carr
//This program calculates the probability of a hand in poker being a "four-of-a-kind",
//"three-of-a-kind", "two-pair", or "one-pair"

import java.util.Scanner;
public class Hw05
{
    public static void main (String [] args)
    {
        Scanner keyboard = new Scanner(System.in);//declares scanner as keyboard
        System.out.println("How many hands would you like to generate? "); //Asks user for number of hands to generate
        int numHands;//declares numHands
        while (true)
        {
          boolean correct = keyboard.hasNextInt(); //Goes into loop and checks if int is entered
          if (correct)
          {
            numHands = keyboard.nextInt(); //IF int is enetered, the int is printed
            break; //used to leave if statement/loop
          }
          else //when the incorrect type is entered
          {
            System.out.println("Please enter the correct type.");
            keyboard.next(); //commands the user to try again
          }
        }
        
        int numHandsCounter = numHands; //creates a counter for numhands
        int card1;//All five card delarations
        int card2;
        int card3;
        int card4;
        int card5;
        
        double numFourKind = 0;//All four hand type counter declarations
        double numThreeKind = 0;
        double numTwoPair = 0;
        double numOnePair = 0;
        
        while (numHandsCounter > 0)
        {
            card1 = (int) (Math.random() * 52) + 1;//randomly gets a card for card1
            card2 = (int) (Math.random() * 52) + 1;
            while (card2 == card1)//checks to see if card1 and card2 are the same, if so generates another number until they are different
            {
                card2 = (int) (Math.random() * 52) + 1 ;
            }
            card3 = (int) (Math.random() * 52) + 1;
            while (card3 == card1 || card3 == card2)//does same thing with card3 and checks against card1 and card2
            {
                card3 = (int) (Math.random() * 52) + 1;
            }
            card4 = (int) (Math.random() * 52) + 1;
            while (card4 == card1 || card4 == card2 || card4 == card3)//does same thing with card4 and checks against cards 1-3
            {
               card4 = (int) (Math.random() * 52) + 1; 
            }
            card5 = (int) (Math.random() * 52) + 1;
            while (card5 == card4 || card5 == card3 || card5 == card2 || card5 == card1)//dooes same thing with card5 and checks against all other cards
            {
                card5 = (int) (Math.random() * 52) + 1;
            }
            
            //uses if statements to test whether a hand matches one of the four criteria
            if ((card1 % 13 == card2 % 13 && card2 % 13 == card3 % 13 && card3 % 13 == card4 % 13) || (card2 % 13 == card3 % 13 && card3 % 13 == card4 % 13 && card4 % 13 == card5 % 13) || (card1 % 13 == card3 % 13 && card3 % 13 == card4 % 13 && card4 % 13 == card5 % 13) || (card1 % 13 == card2 % 13 && card2 % 13 == card4 % 13 && card4 % 13 == card5 % 13) || (card1 % 13 == card2 % 13 && card2 % 13 == card3 % 13 && card3 % 13 == card5 % 13))
            {
                numFourKind++; //if four of the card match then it is incremented
            }
            else if ((card1 % 13 == card2 % 13 && card2 % 13 == card3 % 13 && card3 % 13 != card4 % 13 && card3 % 13 != card5 % 13) || (card2 % 13 == card3 % 13 && card3 % 13 == card4 % 13 && card4 % 13 != card5 % 13 && card4 % 13 != card1 % 13) || (card3 % 13 == card4 % 13 && card4 % 13 == card5 % 13 && card5 % 13 != card1 % 13 && card5 % 13 != card2 % 13) || (card4 % 13 == card5 % 13 && card5 % 13 == card1 % 13 && card1 % 13 != card2 % 13 && card1 % 13 != card3 % 13) || (card5 % 13 == card1 % 13 && card1 % 13 == card2 % 13 && card2 % 13 != card3 % 13 && card2 % 13 != card4 % 13 || (card1 % 13 == card2 % 13 && card2 % 13 == card4 % 13 && card4 % 13 != card3 % 13 && card4 % 13 != card5 % 13) || (card1 % 13 == card3 % 13 && card3 % 13 == card4 % 13 && card4 % 13 != card2 % 13 && card4 % 13 != card5 % 13) || (card1 % 13 == card3 % 13 && card3 % 13 == card5 % 13 && card5 % 13 != card4 % 13 && card5 % 13 != card2 % 13) || (card2 % 13 == card3 % 13 && card3 % 13 == card5 % 13 && card5 % 13 != card4 % 13 && card5 % 13 != card1 % 13) || (card2 % 13 == card4 % 13 && card4 % 13 == card5 % 13 && card5 % 13 != card1 % 13 && card5 % 13 != card3 % 13)))
            {
                numThreeKind++; //if three of the cards match it is incremented, also included full house
            }
            else if ((card1 % 13 == card2 % 13 && card3 % 13 == card4 % 13) || (card1 % 13 == card2 % 13 && card3 % 13 == card5 % 13) || (card1 % 13 == card2 % 13 && card5 % 13 == card4 % 13) || (card1 % 13 == card3 % 13 && card2 % 13 == card4 % 13) || (card1 % 13 == card3 % 13 && card2 % 13 == card5 % 13) || (card1 % 13 == card3 % 13 && card5 % 13 == card4 % 13) || (card1 % 13 == card4 % 13 && card2 % 13 == card3 % 13) || (card1 % 13 == card4 % 13 && card2 % 13 == card5 % 13) || (card1 % 13 == card4 % 13 && card5 % 13 == card3 % 13) || (card2 % 13 == card3 % 13 && card4 % 13 == card5 % 13) || (card1 % 13 == card5 % 13 && card2 % 13 == card3 % 13) || (card1 % 13 == card5 % 13 && card4 % 13 == card3 % 13) || (card1 % 13 == card5 % 13 && card2 % 13 == card4 % 13) || (card3 % 13 == card4 % 13 && card2 % 13 == card5 % 13) || (card3 % 13 == card5 % 13 && card2 % 13 == card4 % 13))
            {
                numTwoPair++; //if there are two pairs of cards that match this is incremented
            }
            else if ((card1 % 13 == card2 % 13 && card2 % 13 != card3 % 13 && card2 % 13 != card4 % 13 && card2 % 13 != card5 % 13) || (card1 % 13 == card3 % 13 && card3 % 13 != card2 % 13 && card3 % 13 != card4 % 13 && card3 % 13 != card5 % 13) || (card1 % 13 == card4 % 13 && card4 % 13 != card2 % 13 && card4 % 13 != card3 % 13 && card4 % 13 != card5 % 13) || (card1 % 13 == card5 % 13 && card5 % 13 != card2 % 13 && card5 % 13 != card3 % 13 && card5 % 13 != card4 % 13) || (card2 % 13 == card3 % 13 && card3 % 13 != card1 % 13 && card3 % 13 != card4 % 13 && card3 % 13 != card5 % 13) || (card2 % 13 == card4 % 13 && card4 % 13 != card1 % 13 && card4 % 13 != card3 % 13 && card4 % 13 != card5 % 13) || (card2 % 13 == card5 % 13 && card5 % 13 != card1 % 13 && card5 % 13 != card3 % 13 && card5 % 13 != card4 % 13) || (card3 % 13 == card4 % 13 && card4 % 13 != card1 % 13 && card2 % 13 != card2 % 13 && card4 % 13 != card5 % 13) || (card3 % 13 == card5 % 13 && card5 % 13 != card1 % 13 && card5 % 13 != card2 % 13 && card5 % 13 != card4 % 13) || (card4 % 13 == card5 % 13 && card5 % 13 != card3 % 13 && card5 % 13 != card2 % 13 && card5 % 13 != card1 % 13))
            {
                numOnePair++; //if there is one pair of cards that match this is incremented, also includes full house
            }
            numHandsCounter--;
        }
        //calculates the probability of all four of the different types of hands
        double probFourKind = numFourKind/numHands;
        double probThreeKind = numThreeKind/numHands;
        double probTwoPair = numTwoPair/numHands;
        double probOnePair = numOnePair/numHands;
        
        //print statements that display the number of loops and the probabilities
        System.out.println("Number of loops genertated: " + numHands);
        System.out.printf("The probability of Four-of-a-kind: %2.3f" , probFourKind );
        System.out.println();
        System.out.printf("The probability of Three-of-a-kind: %2.3f" , probThreeKind);
        System.out.println();
        System.out.printf("The probability of Two-pair: %2.3f" , probTwoPair);
        System.out.println();
        System.out.printf("The probability of One-pair: %2.3f" , probOnePair);
        System.out.println();
        
        
        
    }
}