//Adam Debus CSE2 
//Prof. Carr   9/6/18
//This class calculates the distance and time of a bicycle trip in miles and minutes using rotaions of a 27in wheel

public class Cyclometer{
  
  public static void main (String[] args){
    
    int secsTrip1=480;  //this is the time in seconds of trip1
    int secsTrip2=3220;  //this is the time in seconds of trip2
		int countsTrip1=1561;  //this is the number of rotations of the wheel on trip1
		int countsTrip2=9037; //this is the number of rotations of the wheel on trip2
    
    //constants used in calculations
    double wheelDiameter = 27.0,  //diameter of the wheel
  	PI = 3.14159, // value of pi
  	feetPerMile = 5280,  // conversion of feet to miles
  	inchesPerFoot = 12,   // conversion of inches to feet
  	secondsPerMinute = 60;  // conversion of seconds to minutes
	  double distanceTrip1, distanceTrip2, totalDistance;  //distances of each trip, combined distances
    
    //Prints out information of trip1 (minutes and counts)
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
    //Prints out information of trip2 (minutes and counts)
	  System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2+" counts.");
    
    distanceTrip1 = countsTrip1 * wheelDiameter * PI;
    // Above gives distance in inches
    // (for each count, a rotation of the wheel travels the diameter in inches times PI) = circumference 
	  distanceTrip1 /= inchesPerFoot * feetPerMile; // Gives distance in miles
	  distanceTrip2 = countsTrip2 * wheelDiameter * PI / (inchesPerFoot * feetPerMile); // same calculation as above
	  totalDistance = distanceTrip1 + distanceTrip2;//calculates total diantace of both trips combined
    
    //Prints out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
	  System.out.println("Trip 2 was "+distanceTrip2+" miles");
	  System.out.println("The total distance was "+totalDistance+" miles");

  }
}