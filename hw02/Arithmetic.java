// Adam Debus CSE2
// September 6th, 2018
// This code uses hard coded input to calculate the cost of a trip to the store including PA sales tax

public class Arithmetic{
  
  public static void main (String[] args){
    
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of shirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;
    
    //total cost of pants
    double totalcostPants = numPants * pantsPrice;
    //total cost of sweatshirts
    double totalcostShirts = numShirts * shirtPrice;
    //total cost of belts
    double totalcostBelts = numBelts + beltCost;
    
    //sales tax charged on all pants
    double salestaxPants = (int)(totalcostPants * paSalesTax * 100) / 100.0;
    //sales tax charged on all shirts
    double salestaxShirts = (int)(totalcostShirts * paSalesTax * 100) / 100.0;
    //sales tax charged on all belts
    double salestaxBelts = (int)(totalcostBelts * paSalesTax * 100) / 100.0;
    
    //total cost of entire purchase before tax
    double totalCost = totalcostPants + totalcostShirts + totalcostBelts;
    
    //total sales tax on entire purchase
    double totalSalesTax = (int)(totalCost * paSalesTax * 100) / 100.0;
    
    //total cost of entire purchase including PA sales tax
    double totalCostTax = (int)(((totalCost * paSalesTax) + totalCost) * 100) / 100.0;
    
    //print statements to display the price of each item available for purchase
    System.out.println("The cost of each pair of pants is $" + pantsPrice);
    System.out.println("The cost of each shirt is $" + shirtPrice);
    System.out.println("The cost of each belt is $" + beltCost);
    
    //displays the total sales tax for buying each number of items
    System.out.println("The total sales tax for all of the pants purchased is $" + salestaxPants);
    System.out.println("The total sales tax for all of the shirts purchased is $" + salestaxShirts);
    System.out.println("The total sales tax for all of the belts purchased is $" + salestaxBelts);
    
    //displays the total cost of the entire purchase before tax
    System.out.println("The total cost of the entire purchase before sales tax is $" + totalCost);
    
    //displays the total sales tax
    System.out.println("The total sales tax of the entire purchase altogether is $" + totalSalesTax);
    
    //displays the total cost of the entire purchase including sales tax
    System.out.println("The total cost of the entire purchase including sales tax is $" + totalCostTax);
    
   
  }
}