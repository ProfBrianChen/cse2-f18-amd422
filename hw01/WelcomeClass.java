/// Adam Debus CSE2 
/// August 31st, 2018
/// This class displays a welcome message along with my lehigh email and a small autobiography
public class WelcomeClass{
  

public static void main(String Args[]){
  
  System.out.println("    -----------");
  System.out.println("    | Welcome |");
  System.out.println("    -----------");
  System.out.println();
  
  System.out.println("   ^  ^  ^  ^  ^  ^  ");
  System.out.println("  / \\/ \\/ \\/ \\/ \\/ \\");
  System.out.println(" <-A--M--D--4--2--2-> ");
  System.out.println("  \\ /\\ /\\ /\\ /\\ /\\ /");
  System.out.println("   v  v  v  v  v  v  ");
                     
  System.out.println();
  System.out.println("Hello, I'm Adam. Some of my hobbies include soccer, skiing, mountain biking, and frisbee. I'm in the college of engineering at Lehigh.");
                     
                     }
}
