//Adam Debus
//Prof. Carr CSE002 9/20/18
//This program determines the slang term in craps of a roll of two dice using switch statements

import java.util.Scanner;//Imports scanner
public class CrapsSwitch
{
  
  public static void main (String [] args)
  {
    Scanner keyboard = new Scanner(System.in); //Declared scanner as keyboard
    System.out.println("Would you like to randomly cast dice or state the two dice you want to evaluate. Type '1' for random, '2' for self input. ");
    int yesOrNo = keyboard.nextInt(); ///Decalares variables to be usde in rest of code
    int dice1;
    int dice2;
    
    switch (yesOrNo)//Determines if user uses random or self generated numbers
    {
      case 1:
        dice1 = (int)((Math.random()*6)+1);//Randomly generates first number
        dice2 = (int)((Math.random()*6)+1);//Randomly generates second number
        System.out.println("Your generated numbers are " + dice1 + " and " + dice2);//Tells user their numbers
        
        switch (dice1+dice2)//Adds the dice as a way to differentiate the pairs
        {
          case (2)://Depending on the sum of the numbers there will be a corresponding slang term from lines 26 - 90
            System.out.println("Snake eyes!");
            break;
          case (3):
            System.out.println("Ace Deuce!");
            break;
          case (4):
            switch(Math.abs(dice1-dice2))// Used absolute value for account for the negative when recieving a lower number as dice1
            {
              case (0):
                System.out.println("Hard Four!");
                break;
              case (2):
                System.out.println("Easy Four!");
                break;
            }
            break;
          case (5):
            System.out.println("Fever Five!");
            break;
          case (6):
            switch(Math.abs(dice1-dice2))
            {
              case (0):
                System.out.println("Hard Six!");
                break;
              default://Defaults to any combination other than 3, 3
                System.out.println("Easy Six!");
                break;
            }
            break;
          case (7):
            System.out.println("Seven Out!");
            break;
          case (8):
            switch(Math.abs(dice1-dice2))
            {
              case (0):
                System.out.println("Hard Eight!");
                break;
              default: //Defaults to any combination other than 4, 4
                System.out.println("Easy Eight!");
                break;
            }
            break;
          case (9):
            System.out.println("Nine.");
            break;
          case (10):
            switch(Math.abs(dice1-dice2))
            {
              case (0):
                System.out.println("Hard Ten!");
                break;
              default:
                System.out.println("Easy Ten!");
                break;
            }
            break;
          case (11):
            System.out.println("Yo-leven");
            break;
          case (12):
            System.out.println("Boxcar!");
            break;
        }
        break;//Breaks from randomly generated numbers
        
      case (2)://Evaluates when user chooses to input numbers
        System.out.println("Enter your number for dice 1: ");//Asks for dice1
        dice1 = keyboard.nextInt();//Takes in dice1
        System.out.println("Enter your number for dice 2: ");//Asks for dice2
        dice2 = keyboard.nextInt();//Takes in dice2
        
        switch ((dice1/7) + (dice2)/7)//Makes sure the dice are within range on a standard die
        {
          case 0:
        
        switch (dice1+dice2)//Same method as above for dterminig the slang term, lines 104-170
        {
          case (2):
            System.out.println("Snake eyes!");
            break;
          case (3):
            System.out.println("Ace Deuce!");
            break;
          case (4):
            switch(Math.abs(dice1-dice2))//Absolute value used in the same way as above (to account for dice1 being lower than dice2)
            {
              case (0):
                System.out.println("Hard Four!");
                break;
              case (2):
                System.out.println("Easy Four!");
                break;
            }
            break;
          case (5):
            System.out.println("Fever Five!");
            break;
          case (6):
            switch(Math.abs(dice1-dice2))
            {
              case (0):
                System.out.println("Hard Six!");
                break;
              default:
                System.out.println("Easy Six!");
                break;
            }
            break;
          case (7):
            System.out.println("Seven Out!");
            break;
          case (8):
            switch(Math.abs(dice1-dice2))
            {
              case (0):
                System.out.println("Hard Eight!");
                break;
              default:
                System.out.println("Easy Eight!");
                break;
            }
            break;
          case (9):
            System.out.println("Nine.");
            break;
          case (10):
            switch(Math.abs(dice1-dice2)){
            
              case (0):
                System.out.println("Hard Ten!");
                break;
              case (2):
                System.out.println("Easy Ten!");
                break;
            }
            break;
          case (11):
            System.out.println("Yo-leven");
            break;
          case (12):
            System.out.println("Boxcar!");
            break;
        }
        break;
            
        default://Used when the user inputs numbers that are greater than 6
          System.out.println("Not possinile on a standard die.");
          break;
        }
            break; //Exits code
    }     
        
  }
}