//Adam Debus
//Prof. Carr CSE002 9/20/18
//This program determines the slang term in craps of a roll of two dice using if's

import java.util.Scanner;
public class CrapsIf{
  
  public static void main (String[] args){
    
    Scanner keyboard = new Scanner(System.in); //Declared scanner as keyboard
    System.out.println("Would you like to randomly cast dice or state the two dice you want to evaluate. Type '1' for random, '2' for self input. ");
    int yesOrNo = keyboard.nextInt(); ///Decalares variables to be usde in rest of code
    int dice1;
    int dice2;
    
    if (yesOrNo == 1){//Determines if user wnats randomly generated numbers
      dice1 = (int)((Math.random()*6)+1);//Randomly generates first number
      dice2 = (int)((Math.random()*6)+1);//Randomly generates second number
      System.out.println("Your generated numbers are " + dice1 + " and " + dice2);//Tells user their numbers
      
      if ((dice1 == 1 && dice2 == 1) || (dice2 == 1 && dice1 == 1)){//checks for double 1's
        System.out.println("Snake Eyes!");
      }
      else if ((dice1 == 2 && dice2 == 2) || (dice2 == 2 && dice1 == 2)){//Checks for double 2's
        System.out.println("Hard Four!");
      }
      else if ((dice1 == 3 && dice2 == 3) || (dice2 == 3 && dice1 == 3)){//checks for double 3's
        System.out.println("Hard Six!");
      }
      else if ((dice1 == 4 && dice2 == 4) || (dice2 == 4 && dice1 == 4)){//Checks for double 4's
        System.out.println("Hard Eight!");
      }
      else if ((dice1 == 5 && dice2 == 5) || (dice2 == 5 && dice1 == 5)){//Checks for double 5's
        System.out.println("Hard Ten!");
      }
      else if ((dice1 == 6 && dice2 == 6) || (dice2 == 6 && dice1 == 6)){//Checks for double 6's
        System.out.println("Boxcar!");
      }
      else if ((dice1 == 1 && dice2 == 2) || (dice2 == 1 && dice1 == 2)){// Checks for 1 and 2
        System.out.println("Ace Deuce!");
      }
      else if ((dice1 == 3 && dice2 == 1) || (dice2 == 3 && dice1 == 1)){//Checks for 3 and 1
        System.out.println("Easy Four!");
      }
      else if ((dice1 == 6 && dice2 == 4) || (dice2 == 6 && dice1 == 4)){//Continues to check for designated numbers shown in code until line 64
        System.out.println("Easy Ten!");
      }
      else if ((dice1 == 6 && dice2 == 5) || (dice2 == 6 && dice1 == 5)){
        System.out.println("Yo-leven!");
      }
      else if (((dice1 == 4 && dice2 == 1) || (dice2 == 4 && dice1 == 1)) || ((dice1 == 3 && dice2 == 2) || (dice2 == 3 && dice1 == 2))){
        System.out.println("Fever Five!");
      }
      else if (((dice1 == 5 && dice2 == 1) || (dice2 == 5 && dice1 == 1)) || ((dice1 == 4 && dice2 == 2) || (dice2 == 4 && dice1 == 2))){
        System.out.println("Easy Six!");
      }
      else if (((dice1 == 6 && dice2 == 2) || (dice2 == 6 && dice1 == 2)) || ((dice1 == 5 && dice2 == 3) || (dice2 == 5 && dice1 == 3))){
        System.out.println("Easy eight");
      }
      else if (((dice1 == 6 && dice2 == 3) || (dice2 == 6 && dice1 == 3)) || ((dice1 == 5 && dice2 == 4) || (dice2 == 5 && dice1 == 4))){
        System.out.println("Nine.");
      }
      else if (((dice1 == 6 && dice2 == 1) || (dice2 == 6 && dice1 == 1)) || ((dice1 == 5 && dice2 == 2) || (dice2 == 5 && dice1 == 2)) || ((dice1 == 4 && dice2 == 3) || (dice2 == 4 && dice1 == 3))){
        System.out.println("Seven Out!");
      }
        
    }
    else if (yesOrNo == 2){//Occurs when the user selects to manually input numbers
      System.out.println("Enter your number for dice 1: ");//Asks for dice1
      dice1 = keyboard.nextInt();//Takes in dice1
      System.out.println("Enter your number for dice 2: ");//Asks for dice2
      dice2 = keyboard.nextInt();//Takes in dice2
      
      //Linbe 75 to 120 is the same as above, same technique used to check for number pairs
      if ((dice1 < 7) && (dice1>=1) && (dice2 < 7 && dice2 >=1)){//This line checks to make sure that the number entered is between 1 and 6
        
        if ((dice1 == 1 && dice2 == 1) || (dice2 == 1 && dice1 == 1)){
          System.out.println("Snake Eyes!");
        }
        else if ((dice1 == 2 && dice2 == 2) || (dice2 == 2 && dice1 == 2)){
          System.out.println("Hard Four!");
        }
        else if ((dice1 == 3 && dice2 == 3) || (dice2 == 3 && dice1 == 3)){
          System.out.println("Hard Six!");
        }
        else if ((dice1 == 4 && dice2 == 4) || (dice2 == 4 && dice1 == 4)){
          System.out.println("Hard Eight!");
        }
        else if ((dice1 == 5 && dice2 == 5) || (dice2 == 5 && dice1 == 5)){
          System.out.println("Hard Ten!");
        }
        else if ((dice1 == 6 && dice2 == 6) || (dice2 == 6 && dice1 == 6)){
          System.out.println("Boxcar!");
        }
        else if ((dice1 == 1 && dice2 == 2) || (dice2 == 1 && dice1 == 2)){
          System.out.println("Ace Deuce!");
        }
        else if ((dice1 == 3 && dice2 == 1) || (dice2 == 3 && dice1 == 1)){
          System.out.println("Easy Four!");
        }
        else if ((dice1 == 6 && dice2 == 4) || (dice2 == 6 && dice1 == 4)){
          System.out.println("Easy Ten!");
        }
        else if ((dice1 == 6 && dice2 == 5) || (dice2 == 6 && dice1 == 5)){
          System.out.println("Yo-leven!");
        }
        else if (((dice1 == 4 && dice2 == 1) || (dice2 == 4 && dice1 == 1))||((dice1 == 3 && dice2 == 2) || (dice2 == 3 && dice1 == 2))){
          System.out.println("Fever Five!");
        }
        else if (((dice1 == 5 && dice2 == 1) || (dice2 == 5 && dice1 == 1))||((dice1 == 4 && dice2 == 2) || (dice2 == 4 && dice1 == 2))){
          System.out.println("Easy Six!");
        }
        else if (((dice1 == 6 && dice2 == 2) || (dice2 == 6 && dice1 == 2))||((dice1 == 5 && dice2 == 3) || (dice2 == 5 && dice1 == 3))){
          System.out.println("Easy eight");
        }
        else if (((dice1 == 6 && dice2 == 3) || (dice2 == 6 && dice1 == 3))||((dice1 == 5 && dice2 == 4) || (dice2 == 5 && dice1 == 4))){
          System.out.println("Nine.");
        }
        else if (((dice1 == 6 && dice2 == 1) || (dice2 == 6 && dice1 == 1))||((dice1 == 5 && dice2 == 2) || (dice2 == 5 && dice1 == 2))||((dice1 == 4 && dice2 == 3) || (dice2 == 4 && dice1 == 3))){
          System.out.println("Seven Out!");
      }
      }
      else//Outputs when a number is not 1-6
        System.out.println("Not possible on a standard die");
    }
    
  }
}
