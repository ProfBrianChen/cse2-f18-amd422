//Adam Debus 10/4/18
//Prof. Carr CSE2
//This program asks the user for credentials about a class he or she is taking. 
//Then it asks the user to fill in those credentials using correct types.

import java.util.Scanner;
public class userInput
{
    public static void main (String[] args)
    {
        Scanner keyboard = new Scanner(System.in); //Declares scanner as keyboard
        System.out.println("Please enter the characteristics of one class you are taking as they are asked to you.");//Gives the user instructions
        System.out.println();//Prints a gap between the intructions and next statement for clarity
        
        
        System.out.println("Please enter the course number.");//Asks the user for course number
        while (true)
        {
          boolean correct = keyboard.hasNextInt(); //Goes into loop and checks if int is entered
          if (correct)
          {
            int num = keyboard.nextInt(); //IF int is enetered, the int is printed
            System.out.println("The course number is " + num);
            break; //used to leave if statement/loop
          }
          else //when the incorrect type is entered
          {
            System.out.println("Please enter the correct type.");
            keyboard.next(); //commands the user to try again
          }
         }
      
        //Same steps and comments as above, however this time it asks for a string instead of int
        System.out.println("Please enter the department name.");
        while (true)
        {
          boolean correct = keyboard.hasNext(); //Goes into loop and checks if string is entered
          if (correct)
          {
            String name = keyboard.next();
            System.out.println("The department name is " + name);
            break;
          }
          else
          {
            System.out.println("Please enter the correct type.");
            keyboard.next();//commands the user to try again
          }
         }
      
        //Same steps and comments as above, however this time it asks for an int instead of string
        System.out.println("Please enter the number of times it meets per week.");
        while (true)
        {
          boolean correct = keyboard.hasNextInt(); //Goes into loop and checks if int is entered
          if (correct)
          {
            int num = keyboard.nextInt();
            System.out.println("The number of times the class meets poer week is " + num);
            break;
          }
          else
          {
            System.out.println("Please enter the correct type.");
            keyboard.next();//commands the user to try again
          }
         } 
      
      //Same steps and comments as above, however this time it asks for an int
        System.out.println("Please enter the time of day the course begins. Format: '2200' for 10:00pm");
        while (true)
        {
          boolean correct = keyboard.hasNextInt(); //Goes into loop and checks if int is entered
          if (correct)
          {
            int num = keyboard.nextInt();
            System.out.println("The time of day the couse begins is " + num);
            break;
          }
          else
          {
            System.out.println("Please enter the correct type.");
            keyboard.next();//commands the user to try again
          }
         } 
      
      //Same steps and comments as above, however this time it asks for a string instead of int
        System.out.println("Please enter the instructor name.");
        while (true)
        {
          boolean correct = keyboard.hasNext(); //Goes into loop and checks if string is entered
          if (correct)
          {
            String name = keyboard.next();
            System.out.println("The instructors name is " + name);
            break;
          }
          else
          {
            System.out.println("Please enter the correct type.");
            keyboard.next();//commands the user to try again
          }
         } 
      
      //Same steps and comments as above, however this time it asks for an int
        System.out.println("Please enter the number of students in the class.");
        while (true)
        {
          boolean correct = keyboard.hasNextInt(); //Goes into loop and checks if int is entered
          if (correct)
          {
            int num = keyboard.nextInt();
            System.out.println("The number of students in the class is " + num);
            break;
          }
          else
          {
            System.out.println("Please enter the correct type.");
            keyboard.next();//commands the user to try again
          }
         } 
          
        
         
      
      
      
      
      
      
      
      
      
      } 
}